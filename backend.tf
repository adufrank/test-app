//backend.tf
terraform {
  backend "s3" {
    encrypt = true
    key     = "terraform-pipeline/terraform.tfstate"
    region  = "eu-west-2"
  }
}